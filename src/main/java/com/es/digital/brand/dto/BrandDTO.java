package com.es.digital.brand.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class BrandDTO implements Serializable {
	
	private static final long serialVersionUID = -8389745112869132075L;
	
	private String brandID;
	private String brandName;
	private Date dateCreation;
	private String userCreation;
	private Date dateModified;
	private String userModified;

}
