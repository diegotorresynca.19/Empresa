package com.es.digital.brand.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.es.digital.brand.domain.entity.Brand;
import com.es.digital.brand.service.BrandService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/brand")
@Validated
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(value = "Marcas de Empresa", description = "Operaciones para las marcas")
public class BrandController {
	
	@Autowired
	private BrandService brandService;
	
	@ApiOperation(
			value = "Buscar todas las Marcas.",
			nickname = "Buscar todas las Marcas.",
			response = Long.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successt"),
			@ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 401, message = "Not Authorized"),
			@ApiResponse(code = 403, message = "Accessing forbidden"),
			@ApiResponse(code = 404, message = "Not Found") })
	@GetMapping("/all")
	public ResponseEntity<List<Brand>> getAllBrands(){
		List<Brand> brandList = brandService.getAllById();
		return new ResponseEntity<>(brandList, HttpStatus.OK);
	}
	
	@ApiOperation(
			value = "Crear las Marcas.",
			nickname = "Crears las Marcas.",
			response = Brand.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successt"),
			@ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 401, message = "Not Authorized"),
			@ApiResponse(code = 403, message = "Accessing forbidden"),
			@ApiResponse(code = 404, message = "Not Found") })
	@PostMapping(
			value = "/crear",
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Brand> saveBrand(@RequestBody Brand brand) {
		Brand saveBrand = brandService.saveBrand(brand);
		return new ResponseEntity<>(saveBrand, HttpStatus.CREATED);
	}
	
}
