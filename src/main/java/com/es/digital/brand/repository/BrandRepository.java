package com.es.digital.brand.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.es.digital.brand.domain.entity.Brand;

public interface BrandRepository extends JpaRepository<Brand, Long>{

}
