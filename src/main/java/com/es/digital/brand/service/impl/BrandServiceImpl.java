package com.es.digital.brand.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.es.digital.brand.domain.entity.Brand;
import com.es.digital.brand.repository.BrandRepository;
import com.es.digital.brand.service.BrandService;

@Service
public class BrandServiceImpl implements BrandService{
	
	private BrandRepository brandRepository;
	
	public BrandServiceImpl(BrandRepository brandRepository) {
		this.brandRepository = brandRepository;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Brand> getAllById() {
		return brandRepository.findAll();
	}

	@Override
	public Brand saveBrand(Brand brand) {
		
		return brandRepository.save(brand);
	}

}
