package com.es.digital.brand.service;

import java.util.List;

import com.es.digital.brand.domain.entity.Brand;

public interface BrandService {
	
	public List<Brand> getAllById();
	public Brand saveBrand(Brand brand);

}
