package com.es.digital.brand.domain.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Entity
@Data
@Table(name = "brand")
public class Brand implements Serializable{

	private static final long serialVersionUID = 6611338018428856845L;
	
	@ApiModelProperty(notes = "Id de Marca")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "BRANDID")
	private Long brandID;
	
	@ApiModelProperty(notes = "Nombre de Marca")
	@Column(name = "BRANDNAME")
	private String brandName;
	
	@ApiModelProperty(notes = "Fecha de creacion de Marca")
	@Column(name = "DATECREATION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCreation;
	
	@ApiModelProperty(notes = "Usuario de creacion de Marca")
	@Column(name = "USERCREATION")
	private String userCreation;
	
	@ApiModelProperty(notes = "Fecha modificacion de Marca")
	@Column(name = "DATEMODIFIED")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateModified;
	
	@ApiModelProperty(notes = "Usuario de modificacion de Marca")
	@Column(name = "USERMODIFIED")
	private String userModified;
}
